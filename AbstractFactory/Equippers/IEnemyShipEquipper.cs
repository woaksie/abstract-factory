using AbstractFactory.Parts;

namespace AbstractFactory.Equippers
{
    public interface IEnemyShipEquipper
    {
        ESWeapon addESGun();
        ESEngine addESEngine();
    }
}