using AbstractFactory.Parts;

namespace AbstractFactory.Equippers
{
    public class UfoBossEnemyShipEquipper : IEnemyShipEquipper
    {
        public ESWeapon addESGun()
        {
            return new ESUFOBossGun();
        }

        public ESEngine addESEngine()
        {
            return new ESUFOBossEngine();
        }
    }
}