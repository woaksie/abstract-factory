using AbstractFactory.Parts;

namespace AbstractFactory.Equippers
{
    public class UfoEnemyShipEquipper : IEnemyShipEquipper
    {
        public ESWeapon addESGun()
        {
            return new ESUFOGun();
        }

        public ESEngine addESEngine()
        {
            return new ESUFOEngine();
        }
    }
}