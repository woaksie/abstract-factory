﻿using System;
using AbstractFactory.Factory;
using AbstractFactory.Ships;

namespace AbstractFactory
{
    public class Program
    {
        private static void Main(string[] args)
        {
            EnemyShipBuilderBase makeUfos = new EnemyShipBuilder();

            EnemyShip theGrunt = makeUfos.orderTheShip("UFO");
            EnemyShip theBoss = makeUfos.orderTheShip("UFO BOSS");

            FollowAndAttack(theGrunt);
            FollowAndAttack(theGrunt);
            FollowAndAttack(theBoss);

            Console.ReadKey();
        }

        static void FollowAndAttack(EnemyShip ship)
        {
            ship.displayEnemyShip();
            ship.followHeroShip();
            ship.enemyShipShoots();
        }
    }
}