using System;
using AbstractFactory.Equippers;

namespace AbstractFactory.Ships
{
    public class UfoEnemyShip : EnemyShip
    {
        private readonly IEnemyShipEquipper _shipEquipper;

        public UfoEnemyShip(IEnemyShipEquipper shipEquipper)
        {
            _shipEquipper = shipEquipper;
        }

        public override void equipShip()
        {
            Console.WriteLine("Making enemy ship {0}", name);

            weapon = _shipEquipper.addESGun();
            engine = _shipEquipper.addESEngine();
        }
    }
}