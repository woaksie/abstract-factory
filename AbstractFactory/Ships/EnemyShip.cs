﻿using System;
using AbstractFactory.Parts;

namespace AbstractFactory.Ships
{
    public abstract class EnemyShip
    {
        public string name { get; set; }
        protected ESWeapon weapon { get; set; }
        protected ESEngine engine { get; set; }

        public abstract void equipShip();

        public void followHeroShip()
        {
            Console.WriteLine(name + " is following the hero at " + engine);
        }

        public void displayEnemyShip()
        {
            Console.WriteLine(name + " is on the screen");
        }

        public void enemyShipShoots()
        {
            Console.WriteLine(name + " attacks and does " + weapon);
        }

        public override string ToString()
        {
            return string.Format("The {0} has a top speed {1} and an attack power of {2}", name, engine, weapon);
        }
    }
}