﻿using AbstractFactory.Ships;

namespace AbstractFactory.Factory
{
    public abstract class EnemyShipBuilderBase
    {
        protected abstract EnemyShip makeEnemyShip(string typeOfShip);

        public EnemyShip orderTheShip(string typeOfShip)
        {
            EnemyShip theEnemyShip = makeEnemyShip(typeOfShip);
            theEnemyShip.equipShip();

            return theEnemyShip;
        }
    }
}