﻿using AbstractFactory.Equippers;
using AbstractFactory.Ships;

namespace AbstractFactory.Factory
{
    public class EnemyShipBuilder : EnemyShipBuilderBase
    {
        protected override EnemyShip makeEnemyShip(string typeOfShip)
        {
            EnemyShip theEnemyShip = null;

            switch (typeOfShip)
            {
                case "UFO":
                    {
                        IEnemyShipEquipper shipPartsEquipper = new UfoEnemyShipEquipper();
                        theEnemyShip = new UfoEnemyShip(shipPartsEquipper) {name = "UFO Grunt Ship"};
                    }
                    break;
                case "UFO BOSS":
                    {
                        IEnemyShipEquipper shipPartsEquipper = new UfoBossEnemyShipEquipper();
                        theEnemyShip = new UfoBossEnemyShip(shipPartsEquipper) {name = "UFO Boss Ship"};
                    }
                    break;
            }

            return theEnemyShip;
        }
    }
}